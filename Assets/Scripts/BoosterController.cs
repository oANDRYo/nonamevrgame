﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

class BoosterAction
{
	public Action StartAction
	{
		get;
		private set;
	}

	public Action EndAction
	{
		get;
		private set;
	}

	public float BoosterTime
	{
		get;
		private set;
	}


	public BoosterAction(Action onStart, Action onEnd, float time)
	{
		StartAction = onStart;
		EndAction = onEnd;

		BoosterTime = time;
	}
}

public class BoosterController : MonoBehaviour
{
	private Dictionary<BoosterType, IEnumerator> _activeBoostersCorutine;
	private Dictionary<BoosterType, BoosterAction> _boostersActions;

	void Start()
	{
		_activeBoostersCorutine = new Dictionary<BoosterType, IEnumerator>();


		_boostersActions = new Dictionary<BoosterType, BoosterAction>()
		{
			{BoosterType.ImmortalBooster, new BoosterAction(() => { Debug.Log("StartImmortal");  }, () => { Debug.Log("EndImmortal");  }, 5)},
		};

	}

	public void ActivateBooster(BoosterType boosterType)
	{
		if (IsBoosterNowEnabled(boosterType))
		{
			DisableBooster(boosterType);
		}

		EnableBooster(boosterType);
	}

	public void DeactivateAllBoosters()
	{
		BoosterType[] activeBoosters = null;

		_activeBoostersCorutine.Keys.CopyTo(activeBoosters, 0);

		foreach (BoosterType booster in activeBoosters)
		{
			DisableBooster(booster);
		}
	}

	

	private bool IsBoosterNowEnabled(BoosterType boosterType)
	{
		return _activeBoostersCorutine.ContainsKey(boosterType);
	}

	private void DisableBooster(BoosterType boosterType)
	{
		StopCoroutine(_activeBoostersCorutine[boosterType]);
		_activeBoostersCorutine.Remove(boosterType);

		_boostersActions[boosterType].EndAction();
	}

	private void EnableBooster(BoosterType boosterType)
	{
		IEnumerator corutine = GetCorutine(_boostersActions[boosterType]);

		_activeBoostersCorutine.Add(boosterType, corutine);

		StartCoroutine(CorutineController(corutine, boosterType));
	}

	private IEnumerator CorutineController(IEnumerator corutine, BoosterType boosterType)
	{
		yield return StartCoroutine(corutine);
		_activeBoostersCorutine.Remove(boosterType);
	}

	private IEnumerator GetCorutine(BoosterAction booster)
	{
		if (booster == null)
		{
			Debug.LogError("Dont find booster action in _boostersActions dictionary");
		}

		return StartBooster(booster.StartAction, booster.EndAction, booster.BoosterTime);
	}

	private IEnumerator StartBooster(Action StartMethod, Action EndMethod, float waitTime)
	{
		StartMethod();

		yield return new WaitForSeconds(waitTime);

		EndMethod();

	}
}
