﻿using UnityEngine;

public class Utility
{
	public static T FindElementIfNull<T>(ref T a) where T : Object
	{
		if (a == null)
		{
			a = GameObject.FindObjectOfType<T>();

			if (a == null)
			{
				Debug.LogError("Object with type " + a.GetType() + " not found on scene!");
			}
		}

		return a;
	}
}