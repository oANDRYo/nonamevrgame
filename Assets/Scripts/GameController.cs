﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	private Player _player;
	private Route _route;
	private BoosterController _boosterController;

	void Start()
	{
		//Utility.FindElementIfNull(_player);
		//Utility.FindElementIfNull(_route);
		Utility.FindElementIfNull(ref _boosterController);
	}

	public void BoosterCollision(BoosterType booster)
	{
		_boosterController.ActivateBooster(booster);
	}

	private void BlockCollision(Block block)
	{

	}
}
