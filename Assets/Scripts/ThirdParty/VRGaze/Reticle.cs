using UnityEngine;
using UnityEngine.UI;

namespace VRStandardAssets.Utils
{
    // The reticle is a small point at the centre of the screen.
    // It is used as a visual aid for aiming. The position of the
    // reticle is either at a default position in space or on the
    // surface of a VRInteractiveItem as determined by the VREyeRaycaster.
    public class Reticle : MonoBehaviour
    {
        [SerializeField] private float m_DefaultDistance = 5f;      // The default distance away from the camera the reticle is placed.
        [SerializeField] private bool m_UseNormal;                  // Whether the reticle should be placed parallel to a surface.
        [SerializeField] private Image m_Image;                     // Reference to the image component that represents the reticle.
        [SerializeField] private Transform m_ReticleTransform;      // We need to affect the reticle's transform.
        [SerializeField] private Transform m_Camera;                // The reticle is always placed relative to the camera.

        private Vector3 m_OriginalScale;                            // Since the scale of the reticle changes, the original scale needs to be stored.

        public bool UseNormal
        {
            get { return m_UseNormal; }
            set { m_UseNormal = value; }
        }

        public Transform ReticleTransform { get { return m_ReticleTransform; } }

        private void Awake()
        {
            m_OriginalScale = m_ReticleTransform.localScale;
        }

        public void Hide()
        {
            m_Image.enabled = false;
        }

        public void Show()
        {
            m_Image.enabled = true;
        }

        public void SetPosition ()
        {
            m_ReticleTransform.position = m_Camera.position + m_Camera.forward * m_DefaultDistance;
            m_ReticleTransform.localScale = m_OriginalScale * m_DefaultDistance;
        }

        public void SetPosition (RaycastHit hit)
        {
            m_ReticleTransform.position = hit.point;
            m_ReticleTransform.localScale = m_OriginalScale * hit.distance;
        }
    }
}