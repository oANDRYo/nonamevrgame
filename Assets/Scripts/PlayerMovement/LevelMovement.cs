﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;


public class LevelMovement : MonoBehaviour {

	private Transform _ground;
	private Transform _playerPos;
	private Rigidbody _playerRB;
	private Transform _playerHead;
    //private Pose3D _currentPos;
    private int _currentLine;
	private bool _isMoving;
    private float _groundSpeed=0.1f, _groundDirection, _groundLength=40f, _playerSpeed=0.1f, _stepDistance=3.2f,
        _leftEyeMin = 330f, _leftEyeMax = 270f, _rightEyeMin = 30f, _rightEyeMax = 90f,
        _movementDelay = 0.3f;


	void Start(){
		_ground = GameObject.FindGameObjectWithTag ("Ground").transform;
        GameObject _playerGo = GameObject.FindGameObjectWithTag("Player");
        _playerPos = _playerGo.transform;
        _playerRB = _playerGo.GetComponent<Rigidbody>();
		_playerHead = GameObject.FindGameObjectWithTag("Head").transform;
        _currentLine = 2;
        _isMoving = false;
        _groundDirection = -1f;
        //_currentPos = new Pose3D(_playerHead.position, _playerHead.rotation);
	}

    void FixedUpdate(){
		MoveGround ();
		MovementControl ();
	}

	public void MoveGround(){
        if (_ground.position.z < -_groundLength)
        {
            //reset ground position
            _ground.position = new Vector3(_ground.position.x, _ground.position.y, _groundLength);
        }
		Vector3 dir = new Vector3 (0f, 0f, _groundDirection);
		_ground.Translate (dir * _groundSpeed);
	}

	private void MovementControl(){
		if(GetNextDirection() == "Left" && !_isMoving)
        {
            StartCoroutine(MoveToNextLine(_playerSpeed, "Left"));
        }
        else if (GetNextDirection() == "Right" && !_isMoving)
        {
            StartCoroutine(MoveToNextLine(_playerSpeed, "Right"));
        }
    }

	private string GetNextDirection(){
		if (_playerHead.rotation.eulerAngles.y > _rightEyeMin &&
            _playerHead.rotation.eulerAngles.y < _rightEyeMax
            && _playerPos.position.x < _stepDistance - 0.2f) {
			return "Right";
		}
		if  (_playerHead.rotation.eulerAngles.y < _leftEyeMin &&
            _playerHead.rotation.eulerAngles.y > _leftEyeMax
            && _playerPos.position.x > -_stepDistance - 0.2f) {
			return "Left";
		}
		return "None";
	}

    private void ChangePositionX(float dist)
    {
        _playerRB.MovePosition(new Vector3(_playerPos.position.x + dist,
                            _playerPos.position.y, _playerPos.position.z));
    }

	public IEnumerator MoveToNextLine(float distance, string direction){
        _isMoving = true;

        switch (_currentLine) {
            case 1: //first line
                while (_playerPos.position.x <= 0.0f)
                {
                    ChangePositionX(distance);
                    yield return null;
                }
                _currentLine = 2;
                yield return new WaitForSeconds(_movementDelay);
                break;
            case 2: // second line
                if (direction == "Right") {
                    while (_playerPos.position.x <= _stepDistance)
                    {
                        ChangePositionX(distance);
                        yield return null;
                    }
                    _currentLine = 3;
                    yield return new WaitForSeconds(_movementDelay);
                }
                else if (direction == "Left")
                {
                    while (_playerPos.position.x >= -_stepDistance)
                    {
                        ChangePositionX(-distance);
                        yield return null;
                    }
                    _currentLine = 1;
                    yield return new WaitForSeconds(_movementDelay);
                }
                break;
            case 3: //third line
                while (_playerPos.position.x >= 0.0f)
                {
                    ChangePositionX(-distance);
                    yield return null;
                }
                _currentLine = 2;
                yield return new WaitForSeconds(_movementDelay);
                break;
        }

        _isMoving = false;
    }

}
