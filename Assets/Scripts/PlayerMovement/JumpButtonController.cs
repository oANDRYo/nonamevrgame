﻿using UnityEngine;
using System.Collections;

public class JumpButtonController : MonoBehaviour {

    private float _topBound = 8f;

    void OnTriggerEnter(Collider other)
    {
        Rigidbody rb = other.transform.gameObject.GetComponent<Rigidbody>();
        rb.AddForce(new Vector3(0f, _topBound, 0f), ForceMode.Impulse);
    }


}
