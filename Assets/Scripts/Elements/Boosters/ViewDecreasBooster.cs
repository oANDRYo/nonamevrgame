﻿using UnityEngine;
using System.Collections;
using System;

public class SpeedUpBooster : Booster
{
	protected override BoosterType GetBoosterType()
	{
		return BoosterType.ImmortalBooster;
	}
}
