﻿using UnityEngine;
using System.Collections;
using System;

public class ViewDecreasBooster : Booster
{
	protected override BoosterType GetBoosterType()
	{
		return BoosterType.ImmortalBooster;
	}
}
