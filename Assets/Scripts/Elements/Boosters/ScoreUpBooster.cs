﻿using UnityEngine;
using System.Collections;
using System;

public class ScoreUpBooster : Booster
{
	protected override BoosterType GetBoosterType()
	{
		return BoosterType.ImmortalBooster;
	}
}
