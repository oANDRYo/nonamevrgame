﻿using UnityEngine;
using System.Collections;
using System;

public class ImmortalBooster : Booster
{
	protected override BoosterType GetBoosterType()
	{
		return BoosterType.ImmortalBooster;
	}
}
