﻿
public enum BoosterType : byte
{
	ImmortalBooster,
	ScoreUpBooster,
	SpeedUpBooster,
	ViewDecreasBooster
}