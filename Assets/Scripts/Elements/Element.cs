﻿using UnityEngine;
using System.Collections;

public abstract class Element : MonoBehaviour
{
	protected Transform _cachedTransform;
	protected GameController _gameController;

	public bool ActiveOnScene
	{
		get
		{
			return _cachedTransform.gameObject.activeSelf;
		}
		set
		{
			_cachedTransform.gameObject.SetActive(value);
		}
	}
	
	void Awake()
	{
		Utility.FindElementIfNull(ref _gameController);
		Utility.FindElementIfNull(ref _cachedTransform);
	}

	
}
