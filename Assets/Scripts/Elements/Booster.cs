﻿using UnityEngine;
using System.Collections;

public abstract class Booster : Element {

	protected abstract BoosterType GetBoosterType();

	void OnTriggerEnter(Collider col)
	{
		_gameController.BoosterCollision(GetBoosterType());
	}
	
}
